from django.contrib import admin

from .models import Alcoholtypes, Alcoholbrands, Restaurants, Menu, Alcohols

admin.site.register(Alcoholtypes)
admin.site.register(Alcoholbrands)
admin.site.register(Restaurants)
admin.site.register(Menu)
admin.site.register(Alcohols)
