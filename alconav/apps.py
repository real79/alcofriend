from django.apps import AppConfig


class AlconavConfig(AppConfig):
    name = 'alconav'
