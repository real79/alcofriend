# Generated by Django 3.0.2 on 2020-01-14 11:50

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('alconav', '0004_auto_20200112_1624'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Alcohol_brands',
            new_name='Alcoholbrands',
        ),
        migrations.RenameModel(
            old_name='Alcohol_types',
            new_name='Alcoholtypes',
        ),
    ]
