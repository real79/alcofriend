from rest_framework import status
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response
from rest_framework.validators import UniqueTogetherValidator

class CreateOrUpdateModelMixin(CreateModelMixin):
    """
        mixin проверяет составной ключ модели в случае совпадения обновляет данные этой строки
    """

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            is_key = False
            for key in serializer._errors:
                if "must make a unique set." in serializer._errors[key][0]:
                    is_key = True
                    break
            if is_key:
                myfilter = {}
                for validator in serializer.validators:
                    if isinstance(validator, UniqueTogetherValidator):
                        for field in validator.fields:
                            myfilter[field] = serializer.data.get(field)
                menu_obj = serializer.Meta.model.objects.get(**myfilter)
                serializer.update(menu_obj, serializer.data)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            else:
                return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
