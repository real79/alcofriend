from django.db import models


class Restaurants(models.Model):
    name = models.CharField(max_length=100)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True)
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Alcoholtypes(models.Model):  # whiskey, vodka, wine and etc
    name = models.CharField(max_length=100)

    class Meta:
        unique_together = (('name',),)

    def __str__(self):
        return self.name


class Alcoholbrands(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        unique_together = (('name',),)

    def __str__(self):
        return self.name


class Alcohols(models.Model):
    name = models.CharField(max_length=100)
    type = models.ForeignKey(Alcoholtypes, on_delete=models.CASCADE)
    brand = models.ForeignKey(Alcoholbrands, on_delete=models.CASCADE)


class Menu(models.Model):
    alcohol = models.ForeignKey(Alcohols, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurants, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)

    def field_exists(self, field):
        try:
            self._meta.get_field(field)
            return True
        except models.FieldDoesNotExist:
            return False

    class Meta:
        unique_together = (('alcohol', 'restaurant'),)
