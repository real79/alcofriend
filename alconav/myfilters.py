from rest_framework.filters import BaseFilterBackend
from functools import reduce,partial

class MoreLessFilter(BaseFilterBackend):
    """
    This filter for filtering some decilmal fileds for >(>=) or <(<=) expressions
    """

    template = 'mlfilter.html'

    def get_filter_fields(self, view):
        return getattr(view, 'filter_fields', None)

    def check_for_equalexpr(self, value, expr_template):
        if value == '':  # no equal in experession
            return expr_template
        else:
            return expr_template + 'e'  # equal in experession present

    def checkappend(self, field, value, expr, filter_fields, myfilter):
        field = field.strip()
        value = value.strip()
        if field in filter_fields:
            myfilter[field + expr] = value
        return myfilter

    def split_query_param_func(self, myfilter, expr, filter_fields):
        field = ''
        value = ''
        expr_sign = ''
        if ">" in expr:
            (field, value) = expr.split('>')
            expr_sign = self.check_for_equalexpr(value, '__gt')

        elif "<" in expr:
            (field, value) = expr.split('<')
            expr_sign = self.check_for_equalexpr(value, '__lt')

        return self.checkappend(field, value, expr_sign, filter_fields, myfilter)

    def get_filterexpressions(self, request, view):
        reducer_func=partial(self.split_query_param_func,filter_fields=self.get_filter_fields(view))
        myfilter = reduce(reducer_func, list(request.GET),{})
        return myfilter

    def filter_queryset(self, request, queryset, view):
        myfilter = self.get_filterexpressions(request, view)
        return queryset.filter(**myfilter)

    def get_term(self, myfilter, field):
        if field in myfilter.keys():
            return myfilter[field]
        else:
            return ''
