from cbrf.models import DailyCurrenciesRates
from rest_framework import serializers

from .models import Menu, Alcoholbrands, Alcoholtypes, Alcohols, Restaurants


# class DynamicFieldsModelSerializer(serializers.ModelSerializer):
#     """
#     A ModelSerializer that takes an additional `fields` argument that
#     controls which fields should be displayed.
#     """
#
#     def __init__(self, *args, **kwargs):
#         # Don't pass the 'fields' arg up to the superclass
#         fields = kwargs.pop('fields', None)
#
#         # Instantiate the superclass normally
#         super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)
#
#         # if fields is not None: вот здесь бы надо добавить мои поля
#         #     for field_name in fields:


class AlcoholSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alcohols
        fields = ('pk', 'name', 'brand', 'type')


class AlcoholBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alcoholbrands
        fields = ('name',)


class AlcoholTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alcoholtypes
        fields = ('name',)


class RestaurantSerializer(serializers.ModelSerializer):  # (DynamicFieldsModelSerializer):
    class Meta:
        model = Restaurants
        fields = ('name', 'latitude', 'longitude', 'address')


class MenuSerializer(serializers.ModelSerializer):
    price_in_euro = serializers.SerializerMethodField('get_price_in_euro')
    restaurant = RestaurantSerializer()
    alcohol = AlcoholSerializer()

    def get_price_in_euro(self, obj):
        return obj.price / DailyCurrenciesRates().get_by_id('R01239').value

    def create(self, validated_data):
        return Menu.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.price = validated_data.get('price', instance.price)
        instance.save()
        return instance

    class Meta:
        model = Menu
        fields = ('alcohol', 'restaurant', 'price', 'price_in_euro')
