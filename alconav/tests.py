from django.test import TestCase

from alconav.fortest import CreateTableRows


class TestМеnuView(TestCase):
    def testMenuViewGet(self):
        response = self.client.get('http://testserver/api/menu/')
        assert response.status_code == 200

    def testMenuCreateUpdate(self):
        creator = CreateTableRows()
        creator.create_alcohol()
        creator.create_restaurant()
        response = self.client.post('/api/menu/', {'alcohol': '1', 'restaurant': '1', 'price': '1'})
        assert response.status_code == 201
        response = self.client.post('/api/menu/', {'alcohol': '1', 'restaurant': '1', 'price': '2'})
        assert response.status_code == 202


class TestRestaurantsView(TestCase):
    def testRestaurantsViewGet(self):
        response = self.client.get('/api/restaurants/')
        self.assertEqual(response.status_code, 200)

    def testRestaurantsCreate(self):
        response = self.client.post('/api/restaurants/',
                                    {'name': 'dfsd', 'address': 'dfsdsd', 'latitude': '1', 'longitude': '1'})
        self.assertEqual(response.status_code, 201)


class TestAlcoholView(TestCase):
    def testAlcoholView(self):
        response = self.client.get('/api/alcohol/')
        self.assertEqual(response.status_code, 200)

    def testAlcoholCreate(self):
        creator = CreateTableRows()
        creator.create_alcoholtype()
        creator.create_alcoholbrand()
        response = self.client.post('/api/alcohol/', {'type': '1', 'brand': '1', 'name': 'dfsfs'})
        self.assertEqual(response.status_code, 201)


class TestAlcoholBrandsView(TestCase):
    def testAlcoholbrandsViewGet(self):
        response = self.client.get('/api/alcoholbrands/')
        self.assertEqual(response.status_code, 200)

    def testAlcoholbrandsCreate(self):
        response = self.client.post('/api/alcoholbrands/', {'name': 'dfsd'})
        self.assertEqual(response.status_code, 201)


class TestAlcoholTypesView(TestCase):
    def testAlcoholtypesViewGet(self):
        response = self.client.get('/api/alcoholtypes/')
        self.assertEqual(response.status_code, 200)

    def testAlcoholtypesCreate(self):
        response = self.client.post('/api/alcoholtypes/', {'name': 'dfsd'})
        self.assertEqual(response.status_code, 201)
