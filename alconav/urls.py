from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from alconav.views.alcohol import AlcoholView
from alconav.views.alcoholbrands import AlcoholBrandView
from alconav.views.alcoholtypes import AlcoholTypeView
from alconav.views.doc import Schema_view
from alconav.views.menu import MenuView
from alconav.views.restaurants import RestaurantMixinView

router = DefaultRouter()
router.register(r'alcohol', AlcoholView, basename='user')
router.register(r'alcoholbrands', AlcoholBrandView, basename='user')
router.register(r'alcoholtypes', AlcoholTypeView, basename='user')
router.register(r'restaurants', RestaurantMixinView, basename='user')
router.register(r'menu', MenuView, basename='user')

urlpatterns = (router.urls)
urlpatterns += [
    url(r'doc', Schema_view)
]
