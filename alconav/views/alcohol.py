from rest_framework import viewsets
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin

from alconav.models import Alcohols
from alconav.serializers import AlcoholSerializer


class AlcoholView(viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    http_method_names = ['get', 'delete', 'post']
    queryset = Alcohols.objects.all()
    serializer_class = AlcoholSerializer
    filter_backends = (OrderingFilter, SearchFilter)
    ordering_fields = ('name', 'type', 'brand')
    ordering = ('name',)
    search_fields = ('name', 'type', 'brand')
