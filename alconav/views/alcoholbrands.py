from rest_framework import viewsets
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin

from alconav.models import Alcoholbrands
from alconav.serializers import AlcoholBrandSerializer


class AlcoholBrandView(viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    http_method_names = ['get', 'delete', 'post']
    queryset = Alcoholbrands.objects.all()
    serializer_class = AlcoholBrandSerializer
    filter_backends = (OrderingFilter, SearchFilter)
    ordering_fields = ('name',)
    ordering = ('name',)
