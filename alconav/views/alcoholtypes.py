from rest_framework import viewsets
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin

from alconav.models import Alcoholtypes
from alconav.serializers import AlcoholTypeSerializer


class AlcoholTypeView(viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    http_method_names = ['get', 'delete', 'post']
    queryset = Alcoholtypes.objects.all()
    serializer_class = AlcoholTypeSerializer
    filter_backends = (OrderingFilter, SearchFilter)
    ordering_fields = ('name',)
    ordering = ('name',)
