from rest_framework import viewsets
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin

from alconav.mixins import CreateOrUpdateModelMixin
from alconav.models import Menu
from alconav.myfilters import MoreLessFilter
from alconav.serializers import MenuSerializer


class MenuView(viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateOrUpdateModelMixin):
    http_method_names = ['get', 'delete', 'post']
    queryset = Menu.objects.all().select_related('restaurant')
    serializer_class = MenuSerializer
    filter_backends = (OrderingFilter, SearchFilter, MoreLessFilter)
    ordering_fields = ('alcohol', 'restaurant')
    ordering = ('alcohol',)
    search_fields = ('alcohol', 'restaurant')
    filter_fields = ('price',)
