from rest_framework import viewsets
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.mixins import RetrieveModelMixin, CreateModelMixin, ListModelMixin

from alconav.models import Restaurants
from alconav.myfilters import MoreLessFilter
from alconav.serializers import RestaurantSerializer


class RestaurantMixinView(viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin, CreateModelMixin):
    http_method_names = ['get', 'delete', 'post']
    queryset = Restaurants.objects.all()
    serializer_class = RestaurantSerializer
    filter_backends = (OrderingFilter, SearchFilter, MoreLessFilter) 
    ordering_fields = ('name', 'address')
    ordering = ('-address',)
    additional_field_formula = {'newlati': '1000+latitude', 'newlongi': '1000+longitude'}
    search_fields = ('name', 'address')
    filter_fields = ('latitude', 'longitude')
